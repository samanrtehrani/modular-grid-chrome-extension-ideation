# Modular Grid Chrome Extension

## Communication
[`https://modular-grid.hipchat.com/chat`](https://modular-grid.hipchat.com/chat)

## Beta Release Date
Late August 2016

## Web Site
Pending

## Repository URL
Pending

## Interactions
`esc`: Cycles through the various grids in round-robin fashion.
`cntrl + shift` (in succession): Shows and hides the most important information, which can be populated by the user via the settings section.

## Options
### Grid Position
Options: `fluid` or `fixed`.

### Leading
Options: `36px`, `24px`, or user-specified.

### Columns
#### Width
Options: `75px`, `60px`,`48px` or user-specified.

As a hint, choosing `75px` should generate a `25px` gutter width, choosing`60px` should generate a `20px` gutter width, and `48px` should generate a `16px` gutter width.

#### Color
Options: one of the 141 CSS colors.

### Gutters

#### Width
Options: `25px`, `20px`, `16px`, or user-specified.

**Note**: Gutters are typically 1/3rd the width of a column.

### Margins
Options: `split` or `full`

### Breakpoints
Options: `24`, `18`, `16`, `12`, `8`, `4`

### Opacity
Options: `0.0`–`1.0`

The user should have the option of deciding how opaque the grid columns and leading should be.

### Image Comparator
The Grid should give the user the ability to load an image and place it anywhere in the page’s stacking context. It’s important that the user be made aware of the stacking context. Notify the user of the under “glass” problem when the grid is at the top of the stacking context.

When an image is loaded for comparison, the user should be given the ability to choose whether the page’s content or the comparison image will dictate the height of the page.

### Grid Layer
The Grid should give the user the ability to place the layer anywhere in the page’s stacking context. Again, it’s important that the user be made aware of the stacking context. Notify the user of the under “glass” problem when the grid is at the top of the stacking context.

